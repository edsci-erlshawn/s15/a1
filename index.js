console.log('Hello, World')
// Details
const details = {
    fName: "Erlshawn",
    lName: "Rivera",
    age: 18,
    hobbies : [
        "playing", "Watching", "drawing"
    ] ,
    workAddress: {
        housenumber: "Lot 32, Block A",
        street: "Fedman Suites199 Salcedo StreetLegaspi Village 1200",
        city: "Makati City",
        state: "Metro Manila",
    }
}
const work = Object.values(details.workAddress);
console.log("My First Name is " + details.fName)
console.log("My Last Name is " + details.lName)
console.log(`Yes, I am ${details.fName} ${details.lName}.`)
console.log("I am " + details.age + " years old.")
console.log(`My hobbies are ${details.hobbies.join(', ')}.`);
console.log("I work at " + work.join(", ") + ".");